<section class="signup-area bg-blue white">
		<div class="container">

			<div class="signup-fom col-sm-11 col-sm-offset-1">
				<h3 class="col-sm-6">Sign up For Croatia-Movement Newsletter</h3>
				<form class="sub-fom col-sm-6">
					<div class="form-group">
						<input type="text" class="form-control" placeholder="Your Email......" />
					</div>
					<div class="form-group">
						<button class="btn">Subscribe</button>
					</div>
				</form>
			</div>
	
		</div>
	</section>