<!DOCTYPE html>
<html lang="en" class="broken-image-checker">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!--iPhone from zooming form issue (maximum-scale=1, user-scalable=0)-->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <title>Home</title>

	<link rel="icon" type="image/png" href="images/logo-blog.png">
	
    <!-- Bootstrap --><link href="css/bootstrap.min.css" rel="stylesheet">
	
	<link rel="stylesheet" href="style.css">
	<link rel="stylesheet" href="css/colorized.css">
	<link rel="stylesheet" href="css/animate.css">
	<link rel="stylesheet" href="css/slidenav.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	
	<!-- jQuery -->
	<script src="js/jquery-2.2.4.min.js"></script>
	
	<!--browser selector-->
	<script src="js/css_browser_selector.js" type="text/javascript"></script>
	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
	

  </head>
  <body class="transition nav-plusminus slide-navbar  ">

<header>
	<section class="hdr-area hdr-nav cross-toggle bg-white" data-navitemlimit="7">
		<div class="container">
			<nav class="navbar navbar-default" role="navigation" id="slide-nav">
			  <div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
					<a class="navbar-brand" href="./"><img src="images/logo.png" alt="logo" class="broken-image"/></a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div id="slidemenu">
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					  <ul class="nav navbar-nav navbar-main">
  						<li><a href="festival.php">Festivals</a></li>
						<li><a href="accommodation.php">accommodations</a></li>
						<li><a href="contact.php">CONTACT US</a></li>
                              
						<li><a href="culture-and-wellness.php">Culture & wellness</a></li>
						<li><a href="#!2">Blog</a></li>
						<li><a href="#!2">LOGIN</a></li>
					  </ul>
					  
						

					</div><!-- /.navbar-collapse -->
				</div>
			  </div><!-- /.container-fluid -->
			</nav>
		</div>
	</section>
</header>

<main id="page-content">