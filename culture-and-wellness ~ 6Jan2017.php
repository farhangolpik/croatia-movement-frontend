<title>Culture and Wellness</title>
<meta name="description" content="Culture and Wellness">

<?php include("header.php"); ?>

<section class="slider-area culture-wellness-area hover-ctrl no-ctrl" >
		<div id="carousel-example-generic" class="carousel slide slider--pauseplay" data-ride="carousel" data---interval="false">
		
		  <ol class="carousel-indicators thumbs">
			<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
			<li data-target="#carousel-example-generic" data-slide-to="1"></li>
			<li data-target="#carousel-example-generic" data-slide-to="2"></li>
		  </ol>

		  <!-- Wrapper for slides -->
		  <div class="carousel-inner">

				<div class="item active">
				
				  <img src="images/cultureBnr.jpg" alt="...">
				  <div class="container posrel">
					  <div class="caro-caps">
						<h1>CROATIAN CULTURE & WELLNESS</h1><br/>
						<p>Memories of The Cultural & Wellness Week</p>
						<div class="lnk-btn inline-block more-btn"><a href="#">LINK ROUTING</a></div>
					  </div>
				  </div>
				  
				  
				</div>

		  </div>

		  <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left"></span>
		  </a>
		  <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right"></span>
		  </a>
		  
		  
		</div>
		
		
</section>

<section class="culture-shows-area clrlist">
	<div class="container">
		<div class="culture__shows__main">
			<div class="shows__box col-sm-6">
				<div class="hed">
					<h4>Air center, split, Croatia</h4>
					<h2>Famous <span>shows</span></h2>
					<p>Lorem Ipsum is simply dummy text and type setting</p>
				</div>
				<div class="shows__desc">
					<p>
						Lorem Ipsum is simply dummy text of the printing and type setting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries
					</p>
				</div>
				<div class="shows__btn">
					<a href="#">Explore</a>
				</div>
			</div>
			<div class="shows__slider hover-ctrl fadeft col-sm-6">
				<div id="carousel-example-generic-shows" class="carousel slide slider--pauseplay" data-ride="carousel" data---interval="false">
				  <ol class="carousel-indicators thumbs">
					<li data-target="#carousel-example-generic-shows" data-slide-to="0" class="active"></li>
					<li data-target="#carousel-example-generic-shows" data-slide-to="1"></li>
					<li data-target="#carousel-example-generic-shows" data-slide-to="2"></li>
				  </ol>

				  <!-- Wrapper for slides -->
				  <div class="carousel-inner">


					<div class="item active">
					  <img src="images/showsSlide1.jpg" alt="...">
					</div>

					<div class="item">
					  <img src="images/showsSlide1.jpg" alt="...">
					</div>
					

					<div class="item">
					  <img src="images/showsSlide1.jpg" alt="...">
					</div>
					
				  </div>

				  <a class="left carousel-control" href="#carousel-example-generic-shows" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left"></span>
				  </a>
				  <a class="right carousel-control" href="#carousel-example-generic-shows" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right"></span>
				  </a>
				  
				  
				</div>
			</div>
		</div>
	</div>
</section>

<section class="culture-events-area clrlist"> 
	<div class="container">
		<div class="hed">
			<h4>WE PROVIDE AWESOME DEALS</h4>
			<h2>LATEST <span>EVENTS</span></h2>
			<p>Lorem Ipsum is simply dummy text and typesetting</p>
		</div>
		<div class="events__box col-sm-12">
			<div class="events__inr p0 col-sm-12">
				<div class="events__img p0 col-sm-6">
					<div class="events__img__inr">
						<a href="#"><img src="images/latestEvent1.jpg" alt="latest-events"></a>
					</div>
				</div>
				<div class="events__cont col-sm-6">
					<div class="events__title">
						<h3>MOST SUCCESSFUL CULTURAL FESTIVAL</h3>
					</div>
					<div class="events__date">
						<h5>Written on Friday, 04 Nov 2016 4:21</h5>
					</div>
					<div class="events__desc">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
					</div>
					<div class="events__desc__more">
						<a href="#">Explore</a>
					</div>
				</div>
			</div>
            <div class="events__inr p0 col-sm-12">
				<div class="events__cont col-sm-6">
					<div class="events__title">
						<h3>MOST SUCCESSFUL CULTURAL FESTIVAL</h3>
					</div>
					<div class="events__date">
						<h5>Written on Friday, 04 Nov 2016 4:21</h5>
					</div>
					<div class="events__desc">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
					</div>
					<div class="events__desc__more">
						<a href="#">Explore</a>
					</div>
				</div>
                <div class="events__img p0 col-sm-6">
					<div class="events__img__inr">
						<a href="#"><img src="images/latestEvent2.jpg" alt="latest-events"></a>
					</div>
				</div>
			</div>
            <div class="events__inr p0 col-sm-12">
				<div class="events__img p0 col-sm-6">
					<div class="events__img__inr">
						<a href="#"><img src="images/latestEvent3.jpg" alt="latest-events"></a>
					</div>
				</div>
				<div class="events__cont col-sm-6">
					<div class="events__title">
						<h3>MOST SUCCESSFUL CULTURAL FESTIVAL</h3>
					</div>
					<div class="events__date">
						<h5>Written on Friday, 04 Nov 2016 4:21</h5>
					</div>
					<div class="events__desc">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
					</div>
					<div class="events__desc__more">
						<a href="#">Explore</a>
					</div>
				</div>
			</div>
            <div class="events__inr p0 col-sm-12">
				<div class="events__cont col-sm-6">
					<div class="events__title">
						<h3>MOST SUCCESSFUL CULTURAL FESTIVAL</h3>
					</div>
					<div class="events__date">
						<h5>Written on Friday, 04 Nov 2016 4:21</h5>
					</div>
					<div class="events__desc">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
					</div>
					<div class="events__desc__more">
						<a href="#">Explore</a>
					</div>
				</div>
                <div class="events__img p0 col-sm-6">
					<div class="events__img__inr">
						<a href="#"><img src="images/latestEvent4.jpg" alt="latest-events"></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="culture-trip-area">
		<div class="container">
			
				<div class="hed">
					<h4>WE PROVIDE AWESOME DEALS</h4>
					<h2>Customize Your <span>Trip</span></h2>
					<p>Top 10 Destination of Croatia</p>
				</div>
			
			<div class="clearfix"></div>
				
			<div class="sailbox cul__tripbox col-sm-4">
				<div class="sailbox__inr cul__tripbox__inr">
                    <div class="cul__trip__title">
                        <h3>Surf & Yoga Beach House</h3>
                    </div>
					<div class="sailbox__img cul__tripbox__img">
						<img src="images/sail1.jpg" alt="">
						<div class="sailbox__instant cul__tripbox__instant">INSTANT BOOK</div>
					</div>
					<div class="cul__tripbox__desc">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy</p>
					</div>
					<div class="sailbox__cont cul__tripbox__priceView">	
						<ul class="pul-lft">
							<li class="priceView__startFrom">Start From</li>
							<li class="priceView__price">$540.00</li>
						</ul>
						<ul class="pul-rgt priceView__viewBtn__list">
							<li class="priceView__viewBtn"><a href="#">View Details</a></li>
						</ul>
					</div>
				</div>
			</div>

			<div class="sailbox cul__tripbox col-sm-4">
				<div class="sailbox__inr cul__tripbox__inr">
                    <div class="cul__trip__title">
                        <h3>Croatian Ice Hotel</h3>
                    </div>
					<div class="sailbox__img cul__tripbox__img">
						<img src="images/sail2.jpg" alt="">
						<div class="sailbox__instant cul__tripbox__instant">INSTANT BOOK</div>
					</div>
					<div class="cul__tripbox__desc">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy</p>
					</div>
					<div class="sailbox__cont cul__tripbox__priceView">	
						<ul class="pul-lft">
							<li class="priceView__startFrom">Start From</li>
							<li class="priceView__price">$375.00</li>
						</ul>
						<ul class="pul-rgt priceView__viewBtn__list">
							<li class="priceView__viewBtn"><a href="#">View Details</a></li>
						</ul>
					</div>
				</div>
			</div>

			<div class="sailbox cul__tripbox col-sm-4">
				<div class="sailbox__inr cul__tripbox__inr">
                    <div class="cul__trip__title">
                        <h3>Body Balance Bali</h3>
                    </div>
					<div class="sailbox__img cul__tripbox__img">
						<img src="images/sail3.jpg" alt="">
						<div class="sailbox__instant cul__tripbox__instant">INSTANT BOOK</div>
					</div>
					<div class="cul__tripbox__desc">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy</p>
					</div>
					<div class="sailbox__cont cul__tripbox__priceView">	
						<ul class="pul-lft">
							<li class="priceView__startFrom">Start From</li>
							<li class="priceView__price">$555.00</li>
						</ul>
						<ul class="pul-rgt priceView__viewBtn__list">
							<li class="priceView__viewBtn"><a href="#">View Details</a></li>
						</ul>
					</div>
				</div>
			</div>

			<div class="sailbox cul__tripbox col-sm-4">
				<div class="sailbox__inr cul__tripbox__inr">
                    <div class="cul__trip__title">
                        <h3>Rock The Kabash</h3>
                    </div>
					<div class="sailbox__img cul__tripbox__img">
						<img src="images/sail4.jpg" alt="">
						<div class="sailbox__instant cul__tripbox__instant">INSTANT BOOK</div>
					</div>
					<div class="cul__tripbox__desc">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy</p>
					</div>
					<div class="sailbox__cont cul__tripbox__priceView">	
						<ul class="pul-lft">
							<li class="priceView__startFrom">Start From</li>
							<li class="priceView__price">$540.00</li>
						</ul>
						<ul class="pul-rgt priceView__viewBtn__list">
							<li class="priceView__viewBtn"><a href="#">View Details</a></li>
						</ul>
					</div>
				</div>
			</div>

			<div class="sailbox cul__tripbox col-sm-4">
				<div class="sailbox__inr cul__tripbox__inr">
                    <div class="cul__trip__title">
                        <h3>Yoga & Sailing</h3>
                    </div>
					<div class="sailbox__img cul__tripbox__img">
						<img src="images/sail5.jpg" alt="">
						<div class="sailbox__instant cul__tripbox__instant">INSTANT BOOK</div>
					</div>
					<div class="cul__tripbox__desc">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy</p>
					</div>
					<div class="sailbox__cont cul__tripbox__priceView">	
						<ul class="pul-lft">
							<li class="priceView__startFrom">Start From</li>
							<li class="priceView__price">$659.00</li>
						</ul>
						<ul class="pul-rgt priceView__viewBtn__list">
							<li class="priceView__viewBtn"><a href="#">View Details</a></li>
						</ul>
					</div>
				</div>
			</div>

			<div class="sailbox cul__tripbox col-sm-4">
				<div class="sailbox__inr cul__tripbox__inr">
                    <div class="cul__trip__title">
                        <h3>Tribe</h3>
                    </div>
					<div class="sailbox__img cul__tripbox__img">
						<img src="images/sail6.jpg" alt="">
						<div class="sailbox__instant cul__tripbox__instant">INSTANT BOOK</div>
					</div>
					<div class="cul__tripbox__desc">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy</p>
					</div>
					<div class="sailbox__cont cul__tripbox__priceView">	
						<ul class="pul-lft">
							<li class="priceView__startFrom">Start From</li>
							<li class="priceView__price">$255.00</li>
						</ul>
						<ul class="pul-rgt priceView__viewBtn__list">
							<li class="priceView__viewBtn"><a href="#">View Details</a></li>
						</ul>
					</div>
				</div>
			</div>
           
 
		</div>
	</section>

<section class="gallery-area clrlist">
    <div class="container">
        <div class="hed">
            <h4>WE PROVIDE AWESOME DEALS</h4>
            <h2>Our <span>Galleries</span></h2>
            <p>Lorem Ipsum is simply dummy text and typesetting</p>
		</div>
		<div class="clearfix"></div>
    </div>   
        <div class="gallery-images list-col-5">
            <ul>
                <li><img src="images/gallery1.jpg" alt=""></li>
                <li><img src="images/gallery2.jpg" alt=""></li>
                <li><img src="images/gallery3.jpg" alt=""></li>
                <li><img src="images/gallery4.jpg" alt=""></li>
                <li><img src="images/gallery5.jpg" alt=""></li>
                <li><img src="images/gallery6.jpg" alt=""></li>
                <li><img src="images/gallery7.jpg" alt=""></li>
                <li><img src="images/gallery8.jpg" alt=""></li>
                <li><img src="images/gallery9.jpg" alt=""></li>
                <li><img src="images/gallery10.jpg" alt=""></li>
            </ul>
		</div>
		<div class="clearfix"></div>
  
</section>
	
<?php include("signup-area.php"); ?>
	
	
<?php include("footer.php"); ?>