

<title>Accommodation</title>
<meta name="description" content="Accommodation">

<?php include("header.php"); ?>

<section class="accom-bnr-area accom-dtl-bnr-area">
		<div class="accom-bg accom-bnr">
			<img src="images/accomBnr.jpg" alt=""/>
		</div>	
		<div class="container">
			 <div class="accom-bnr-title text-center">
				 <h2>DISCOVER BEST VACATION RENTALS</h2> 
			 </div>
		</div>
</section>

<section class="property-area">
	<div class="container">
		<div class="prop__lft col-sm-8">
			<div class="prop__box__img">
				<div class="prop__title">
					<h2>Hosted House Trad Robert</h2>
				</div>
				<div class="prop__reviews clrlist">
					<ul>
						<li><i class="fa fa-star"></i></li>
						<li><i class="fa fa-star"></i></li>
						<li><i class="fa fa-star"></i></li>
						<li><i class="fa fa-star-half-o"></i></li>
						<li><i class="fa fa-star-o"></i></li>
						<li>10 Reviews</li>
					</ul>
				</div>
				<div class="prop__image">
					<img src="images/propImg.jpg" alt="property image">
				</div>
			</div>
			<div class="prop__facility clrlist">
				<ul>
					<li class="col-sm-2">
						<div class="faci__icon">
							<i class="fa fa-home" aria-hidden="true"></i>
						</div>
						<div class="faci__name">
							<h5>House</h5>
						</div>
					</li>
					<li class="col-sm-2">
						<div class="faci__icon">
							<i class="fa fa-home" aria-hidden="true"></i>
						</div>
						<div class="faci__name">
							<h5>Entire House</h5>
						</div>
					</li>
					<li class="col-sm-2">
						<div class="faci__icon">
							<i class="fa fa-users" aria-hidden="true"></i>
						</div>
						<div class="faci__name">
							<h5>4 People</h5>
						</div>
					</li>
					<li class="col-sm-2">
						<div class="faci__icon">
							<i class="fa fa-bed" aria-hidden="true"></i>
						</div>
						<div class="faci__name">
							<h5>3 Bedrooms</h5>
						</div>
					</li>
					<li class="col-sm-2">
						<div class="faci__icon">
							<i class="fa fa-bed" aria-hidden="true"></i>
						</div>
						<div class="faci__name">
							<h5>5 Beds</h5>
						</div>
					</li>
					<li class="col-sm-2">
						<div class="faci__icon">
							<i class="fa fa-shower"></i>
						</div>
						<div class="faci__name">
							<h5>1 Bathroom</h5>
						</div>
					</li>
					<div class="clearfix"></div>
				</ul>
			</div>
			<div class="prop__desc">
				<div class="prop__desc__title">
					<h3>Description</h3>
				</div>
				<div class="prop__desc__dtl">
					<p>Immediately upon arrival at this holiday home, you will realise that there is something special about this home. This home is furnished with beautiful details, full of modern design in both architecture and furnishing that are worth experiencing. This home is in a unique location with a panoramic view of the sea. You can reach the waters directly from the property. This home is built to provide a feel-good atmosphere and is very tastefully furnished. Spacious living room opens to the kitchen. Ceiling between the first floor and ground floor is open and provides a special ambience. Fireplace in the living room and bedrooms with adjustable bed bases provide comfort. Bathroom is attached to the bedroom. There is a hot tub and a sauna in the huge, spa-type bathroom. You can take a shower on the terrace, after a swim. You can also enjoy an active holiday on this huge property. The terrace is sunny and covered and surrounds the entire house, from where you can enjoy sun in shade. Enjoy pleasant, cosy hours together in the evening with the barbecue. Rich fishing grounds are available in the neighbourhood. A fish cleaning place is available outdoors, where you can organise and clean your fishes after a successful catch. Charming Sonderborg, ideal for wandering and strolls is located nearby. Several, inviting boutiques are located on the pedestrian zone as well as other cosy cafes, beautiful harbour and fascinating museums. Do not miss a visit the Danfoss Universe Adventure Park. There are over 200 breath-taking attractions in this region. </p>
				</div>
			</div>
		</div>
		
		<div class="prop__rgt col-sm-4">
			<div class="prop__price clrlist p0 col-sm-6">
				<ul class="prop__price__one">
					<li class="prop__usd">USD</li>
					<li class="prop__usd__price"><strike>200</strike></li>
				</ul>
				<ul class="prop__price__three">
					<li class="prop__usd__realPrice">150</li>
				</ul>
				<ul class="prop__price__three">
					<li class="prop__price__avg">Average price per night.</li>
				</ul>
			</div>
			<div class="prop__discount clrlist p0 col-sm-6">
				<ul>
					<li>Discount: 16% Off</li>
				</ul>
			</div>
			<div class="prop__booking fnc-select">
				<form>
					<div class="form-group">
						<div class="input-group">
						  <div class="input-group-addon">
							<i class="fa fa-map-marker"></i>
						  </div>
						  <input type="text" class="form-control" id="exampleInputAmount" placeholder="Where are you going?">
						</div>
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						    <input type="text" class="form-control" data-provide="datepicker" placeholder="Check In">
						</div>
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						    <input type="text" class="form-control" data-provide="datepicker" placeholder="Check Out">
						</div>
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-male"></i>
							</span>
							<select type="text" class="form-control">
								<option class="option">Guests</option>
								<option class="option">1</option>
								<option class="option">2</option>
								<option class="option">3</option>
							</select>
						</div>
						<div class="input-group prop__booking__btn">
							<button type="submit" class="btn btn-primary">Instant Booking</button>
						</div>
					</div>
				</form>
			</div>
			<div class="prop__contact p0 col-sm-12">
				<div class="prop__contact__box col-sm-12">
					<div class="prop__contact__img p0 col-sm-3">
						<div class="prop__img__admin">
							<img src="images/accomContactImage.jpg">
						</div>
					</div>
					<div class="prop__contact__admin col-sm-9">
						<div class="prop__admin__abt clrlist">
							<ul>
								<li><strong>Admin</strong><li>
								<li>Member since Dec 2016<li>
								<li>Admin<li>
							</ul>
						</div>
					</div>
					<div class="prop__contact__quality p0 col-sm-12 clrlist">
						<ul>
							<li class="pul-lft">Response Rate</li>
							<li class="pul-rgt">100%</li>
						</ul>
						<ul>
							<li class="pul-lft">Response Time</li>
							<li class="pul-rgt">a few hours</li>
						</ul>
						<ul>
							<li class="pul-lft">Calendar Updated</li>
							<li class="pul-rgt">1 hour ago</li>
						</ul>
						<div class="clearfix"></div>
						<div class="prop__contact__host">
							<a href="#">Contact Host</a>
						</div>
					</div>
				</div>
			</div>
			<div class="prop__share p0 col-sm-12">
				<div class="prop__share__title">
					<h4>Share this Property</h4>
				</div>
				<div class="prop__share__ico clrlist">
					<ul>
						<li><a href="#"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
						<li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
						<li><a href="#"><i class="fa fa-instagram"></i></a></li>
						<li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
					</ul>
				</div>
			</div>
			<div class="prop__map p0 col-sm-12">
				<div class="prop__map__inr">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2800.6187099942817!2d-122.73276298443893!3d45.41702787910033!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x549573306a5cde5d%3A0x2edd26234e25ab74!2s5200+Meadows+Rd%2C+Lake+Oswego%2C+OR+97035!5e0!3m2!1sen!2s!4v1475138848852" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</div>
</section>


<?php include("signup-area.php"); ?>

<?php include("footer.php"); ?>