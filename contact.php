<?php include("header.php"); ?>

	<section class="inr-bnr-area">
		<div class="section-bg section-img">
				<img src="images/contact-bnr.jpg" alt=""/>
		</div>	
	<div class="container">
	     <div class="inr-cont text-center">
		      <h2>Contact Us</h2> 
		 </div>
	</div>
	</section>

    <section class="contact-area">
	<div class="container">
	     <div class="contact-box contact-padding col-sm-9">
		      <div class="contact__inr">
                   <div class="contact__hed"><h3>SEND US A MESSAGE!</h3></div>
                   <div class="contact__form">
				    <form role="form"> 
					   <div class="input-box col-sm-6">
				       <div class="input-group">
							<span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
							<input type="text" class="form-control" placeholder="Your Name">
                       </div>  
					   </div>
					   <div class="input-box col-sm-6">
				       <div class="input-group">
							<span class="input-group-addon"><i class="fa fa-envelope" aria-hidden="true"></i></span>
							<input type="email" class="form-control" placeholder="Email">
                       </div>
                       </div> 	
					   <div class="input-box col-sm-12">
				       <div class="input-group">
							<span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
							<input type="email" class="form-control" placeholder="Email">
                       </div>
                       </div>
					   <div class="input-box col-sm-12">
				       <div class="input-group">
							<span class="input-group-addon"><i class="fa fa-pencil" aria-hidden="true"></i></span>
							<textarea class="form-control" rows="3" placeholder="Your Message"></textarea>
                       </div>
                       </div>
					   <div class="input-box submit-box col-sm-12">
				       <div class="input-group pull-right">
                            <button type="submit" class="submit-btn btn btn-default">Send</button> 
                       </div>
                       </div>					   
                    </form>
                   </div> 				   
		      </div> 
		 </div>
	     <div class="contact-box col-sm-3">
		      <div class="contact__inr">
                   <div class="contact__hed"><h3>CONTACT INFO</h3></div>
				   <div class="contact__info">
				       <div class="contact__list clrlist listview">
					        <ul>
							    <li><i class="fa fa-map-marker" aria-hidden="true"></i> <strong>Address</strong>
								<br> <span>Kemp House, 152 City Road, London EC1 V2NX</span></li>
							    <li><i class="fa fa-envelope" aria-hidden="true"></i> <strong>Email</strong>
								<br><span><a href="mailto:Info@croatia-movement.com">Info@croatia-movement.com</a></span></li>
							    <li><i class="fa fa-phone" aria-hidden="true"></i> <strong>Phone</strong>
								<br><span><a href="tel:+1 708 680 6962">+1 708 680 6962</a></span></li>								
                            </ul>							

                       </div>
                       <div class="social__list clrlist">
                            <ul>
							    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
							    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
							    <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
							    <li><a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
							    <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>								
							</ul>					   
                       </div>  					   
				   
				   </div> 
		      </div> 
		 </div>		 
	</div>
	</section>
	
	<section class="map-area">
	<div class="container">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2482.23290294351!2d-0.09192238429231206!3d51.527287917069906!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48761ca671c887ed%3A0x35325a4cd49f9074!2sKEMP+HOUSE%2C+City+Rd%2C+London+EC1V+2PD%2C+UK!5e0!3m2!1sen!2s!4v1482326281610" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
	</div>
	</section>
				









	

			
<?php include("signup-area.php"); ?>
	
	
<?php include("footer.php"); ?>