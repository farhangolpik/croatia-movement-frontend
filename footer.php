<footer>
		<section class="ftr-area bg-black white pt30 pb30 ">
			<div class="container">
				<div class="ftr__box col-sm-3 ftr__logo">
					<h4>About Company</h4>
					<div class="ftr-logo gray-img pul-lft"><img src="images/logo.png" alt="ftr logo" /></div>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text</p>
					
					<div class="social-icons clrlist">
						<ul>
							<li><i class="fa fa-facebook"></i></li>
							<li><i class="fa fa-twitter"></i></li>
							<li><i class="fa fa-google"></i></li>
							<li><i class="fa fa-linkedin"></i></li>
							<li><i class="fa fa-instagram"></i></li>
						</ul>
					</div>
					
				</div>


				<div class="ftr__box ftr__links col-sm-3 clrlist listview ">
					<h4>About Us</h4>
					<ul>
						<li><a href="#">About Us</a></li>
						<li><a href="#">Contest</a></li>
						<li><a href="#">Deals</a></li>
						<li><a href="#">Blogs</a></li>
						<li><a href="#">FAQ’S</a></li>
					</ul>
				</div>


				<div class="ftr__box ftr__links col-sm-3 clrlist listview">
					<h4>Help</h4>
					<ul>
						<li><a href="#">Create Events</a></li>
						<li><a href="#">Pricing</a></li>
						<li><a href="#">Contact Us</a></li>
						<li><a href="#">Advertise</a></li>
						<li><a href="#">FAQ’S</a></li>
					</ul>
				</div>


				<div class="ftr__box ftr__links col-sm-3 clrlist listview">
					<h4>Related Links</h4>
					<ul>
						<li><a href="#">Terms & Conditons</a></li>
						<li><a href="#">Privacy & Cookies</a></li>
						<li><a href="#">Contact Us</a></li>
					</ul>
				</div>


			
			</div>
		</section>
		
		
		<section class="bottom-area  bg-gray white p10 text-center">
			<div class="container">

				<div>© 2016 Croatia-Movement All rights reserved.</div>

				<div class="bot__links clrlist listdvr pul-cntr">
					<ul>
						<li><a href="#">Privacy</a></li>
						<li><a href="#">Terms</a></li>
					</ul>
					<div class="bot__des__dev">
						<p>Designed & Developed by <a href="http://www.golpik.com/" target="_blank">GOLPIK Inc.</a></p>
					</div>
				</div>
				
			</div>
		</section>
		
	</footer>
	
			<a href="" class="scrollToTop"><i class="fa fa-arrow-up"></i></a>
	
</main>
    
	<!--Bootstrap-->
    <script src="js/bootstrap.min.js"></script>
	<!--./Bootstrap-->
	
	<!--Major Scripts-->
	<script src="js/viewportchecker.js"></script>
    <script src="js/kodeized.js"></script>
	<!--./Major Scripts-->

	
		</body>
</html>