<?php include("header.php"); ?>

<section class="slider-area hover-ctrl no-ctrl" >
		<div id="carousel-example-generic" class="carousel slide slider--pauseplay" data-ride="carousel" data---interval="false">
		
		  <ol class="carousel-indicators thumbs">
			<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
			<li data-target="#carousel-example-generic" data-slide-to="1"></li>
			<li data-target="#carousel-example-generic" data-slide-to="2"></li>
		  </ol>

		  <!-- Wrapper for slides -->
		  <div class="carousel-inner">

				<div class="item active">
					<video src="images/Croatia-movements.mp4" poster="images/videoPoster.jpg" autoplay loop></video>
				  <div class="container posrel">
					  <div class="caro-caps">
						<h1>Croatia Movement</h1>
						<p>Festivals and Wellness</p>
						<div class="lnk-btn inline-block more-btn"><a href="#">WATCH</a></div>
					  </div>
				  </div>
				  
				  
				</div>

		  </div>

		  <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left"></span>
		  </a>
		  <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right"></span>
		  </a>
		  
		  
		</div>
		
		
	</section>
	
	
	
	
	
	<section class="trip-area imgzoom--hover">
		<div class="container0">
			
				<div class="hed">
					<h4>WE PROVIDE AWESOME DEALS</h4>
					<h2>WHY <span>CROATIA MOVEMENT</span></h2>
					<p>We like to Party but also look after ourselves </p>
				</div>

		
			<div class="clearfix"></div>


			<div class="tripbox col-sm-4">
				<div class="tripbox__inr">
					<div class="tripbox__img">
						<img src="images/festival.jpg" alt="" />
					</div>
					<div class="tripbox__cont">
						<div class="cont hover">
							<h3>FESTIVALS</h3>
						</div>
					</div>
				</div>
			</div>
			


			<div class="tripbox col-sm-4">
				<div class="tripbox__inr">
					<div class="tripbox__img">
						<img src="images/accomodation.jpg" alt="" />
					</div>
					<div class="tripbox__cont">
						<div class="cont hover">
							<h3>ACCOMODATION</h3>
						</div>
					</div>
				</div>
			</div>
			


			<div class="tripbox col-sm-4">
				<div class="tripbox__inr">
					<div class="tripbox__img">
						<img src="images/wellness.jpg" alt="" />
					</div>
					<div class="tripbox__cont">
						<div class="cont hover">
							<h3>WELLNESS</h3>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</section>


	<section class="desti-area transition">
		<div class="container">
			
				<div class="hed">
					<h4>WE PROVIDE AWESOME DEALS</h4>
					<h2>top <span>festivals</span></h2>
					<p>Croatia Movement Select the Trusted Top Festivals</p>
				</div>
			
			<div class="clearfix"></div>


			<div class="desti-tabs-area link-effect--ripple">

				<div class="tab-content overload">

				  <div class="tab-pane active" id="desti1">
						
						<div class="destibox col-sm-4">
							<div class="destibox__inr">
								<div class="destibox__img">
									<img src="images/city3-3-3-3.jpg" alt="" />
								</div>
								<div class="destibox__cont">
									<h2>Hideout</h2>
									<p></p>
								</div>
							</div>
						</div>



						<div class="destibox col-sm-4">
							<div class="destibox__inr">
								<div class="destibox__img">
									<img src="images/city4-4-4-4.jpg" alt="" />
								</div>
								<div class="destibox__cont">
									<h2>Love International</h2>
									<p> </p>
								</div>
							</div>
						</div>



						<div class="destibox col-sm-4">
							<div class="destibox__inr">
								<div class="destibox__img">
									<img src="images/city5-5-5-5.jpg" alt="" />
								</div>
								<div class="destibox__cont">
									<h2>Sonus</h2>
									<p> </p>
								</div>
							</div>
						</div>


				  </div>

				 
				  
				 





						



						
						</div>

				</div>
			</div>


		</div>
	</section>


	<section class="testi-area">
		<div class="container0">
			


			<div id="carousel-example-generic2" class="carousel slide" data-ride="carousel">
			  
			  <ol class="carousel-indicators">
				
				
			  </ol>

			  <div class="carousel-inner">

				<div class="item active">
				  <div class="testi-bg"><img src="images/testimo-bg.jpg" alt="..."></div>
				  <div class="carousel-caption testi-qoutes">
					That yoga retreat was EXACTLY what we needed after a week of partying. - Bronte Buck
				  </div>
				</div>


				



				


			  </div>
 
			  <a class="left carousel-control hidden" href="#carousel-example-generic2" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left"></span>
			  </a>
			  <a class="right carousel-control hidden" href="#carousel-example-generic2" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right"></span>
			  </a>
			</div>



		</div>
	</section>
				




	<section class="sail-area pt70 pb70 ">
		<div class="container">
			
				<div class="hed">
					<h4>WE PROVIDE AWESOME DEALS</h4>
					<h2>TOP THREE  <span>PACKAGES</span></h2>
					<p>Top 10 Destination of Croatia</p>
				</div>
			
			<div class="clearfix"></div>
				
			<div class="sailbox col-sm-4">
				<div class="sailbox__inr">
					<div class="sailbox__img">
						<img src="images/sail1-1-1.jpg" alt="" />
						<div class="sailbox__instant">INSTANT BOOK</div>
						<div class="sailbox__price">From: $ 900.00</div>
					</div>
					<div class="sailbox__cont">	
						<h3>Hideout Festival & Body Balance Spa </h3>
						<div class="cont">Festival Ticket x 4* Apartment (7 days) X Spa Retreat (2 days) 9 Days Total</div>
						<div class="loc">Novalja</div>
					</div>
				</div>
			</div>

			<div class="sailbox col-sm-4">
				<div class="sailbox__inr">
					<div class="sailbox__img">
						<img src="images/sail2-2-2.jpg" alt="" />
						<div class="sailbox__instant">INSTANT BOOK</div>
						<div class="sailbox__price">From: $ 900.00</div>
					</div>
					<div class="sailbox__cont">	
						<h3>Love International & SUP Yoga </h3>
						<div class="cont">Festival Ticket x Apartment (7 days) X Stand up Paddleboard Yoga (2 days) 9 Days Total</div>
						<div class="loc">Tisno & Sibenik (Transport Included)</div>
					</div>
				</div>
			</div>

			<div class="sailbox col-sm-4">
				<div class="sailbox__inr">
					<div class="sailbox__img">
						<img src="images/sail3-3-3.jpg" alt="" />
						<div class="sailbox__instant">INSTANT BOOK</div>
						<div class="sailbox__price">From: $ 900.00</div>
					</div>
					<div class="sailbox__cont">	
						<h3>Sonus & Beach Yoga Recovery </h3>
						<div class="cont">Sonus Festival Ticket x Apartment (7 days) X Beach Yoga Recovery (2 Days) 9 days Total</div>
						<div class="loc">Novalja & Zadar (Transport Included)</div>
					</div>
				</div>
			</div>
 
		</div>
	</section>
				



	<section class="advent-area ">
		<div class="container0">
			
			<div class="adventbox col-sm-6 bg-cvr" style="background-image:url('images/start1.jpg')">
				<div class="container-half lft">
					<h2>START YOUR adventurous TRIP NOW</h2>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry.</p>
					<div class="lnk-btn more-btn"><a href="#">explore</a></div>
				</div>
			</div>

			<div class="adventbox col-sm-6 bg-cvr p0" style="background-image:url('images/start2.jpg')">
				
			</div>

		</div>
	</section>
				



	<section class="instagram-area pt70 pb70">
		<div class="container">
 
				<div class="hed">
					<h4>WE PROVIDE AWESOME DEALS</h4>
					<h2>Instagram <span>Post</span></h2>
					<p>Top 10 Destination of Croatia</p>
				</div>

		</div>

			<div class="clearfix"></div>

			<div class="instagram-feeds list-col-6">
				<ul>
					<li><img src="images/insta1.jpg" alt="" /></li>
					<li><img src="images/insta2.jpg" alt="" /></li>
					<li><img src="images/insta3.jpg" alt="" /></li>
					<li><img src="images/insta4.jpg" alt="" /></li>
					<li><img src="images/insta5.jpg" alt="" /></li>
					<li><img src="images/insta6.jpg" alt="" /></li>
					<li><img src="images/insta7.jpg" alt="" /></li>
					<li><img src="images/insta8.jpg" alt="" /></li>
					<li><img src="images/insta9.jpg" alt="" /></li>
					<li><img src="images/insta10.jpg" alt="" /></li>
					<li><img src="images/insta11.jpg" alt="" /></li>
					<li><img src="images/insta12.jpg" alt="" /></li>
				</ul>
			</div>

	</section>




<?php include("signup-area.php"); ?>
	
	
<?php include("footer.php"); ?>