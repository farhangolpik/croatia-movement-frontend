<title>Culture and Wellness Detail</title>
<meta name="description" content="Culture and Wellness">

<?php include("header.php"); ?>

<section class="slider-area culture-wellness-area hover-ctrl no-ctrl" >
		<div id="carousel-example-generic" class="carousel slide slider--pauseplay" data-ride="carousel" data---interval="false">
		
		  <ol class="carousel-indicators thumbs">
			<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
			<li data-target="#carousel-example-generic" data-slide-to="1"></li>
			<li data-target="#carousel-example-generic" data-slide-to="2"></li>
		  </ol>

		  <!-- Wrapper for slides -->
		  <div class="carousel-inner">

				<div class="item active">
				
				  <img src="images/cultureDtlBnr.jpg" alt="...">
				  <div class="container posrel">
					  <div class="caro-caps">
						<h1>CROATIAN CULTURE & WELLNESS</h1><br/>
						<p>Memories of The Cultural & Wellness Week</p>
						<div class="lnk-btn inline-block more-btn"><a href="#">LINK ROUTING</a></div>
					  </div>
				  </div>
				</div>

		  </div>

		  <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left"></span>
		  </a>
		  <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right"></span>
		  </a>
		  
		  
		</div>
		
		
</section>

<section class="famous-show clrlist">
	<div class="container">
		<div class="hed">
			<h4>Air center, split, Croatia</h4>
			<h2>Famous Floating <span>shows</span></h2>
			<p>Lorem Ipsum is simply dummy text and type setting</p>
		</div>
		<div class="clearfix"></div>
		
		<div class="famshow__desc col-sm-12">
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktopLorem Ipsum.</p>
		</div>
		<div class="clearfix"></div>
		<div class="famshow__more">
			<a href="#">Explore</a>
		</div>
	</div>
</section>

<section class="shows-gallery-area clrlist">
	<div class="container0">
		<div class="shows__gallery__box p0 col-sm-12">
			<div class="shows__gallery__box__inr p0 col-sm-12">
				<div class="shows__gallery__lft p0 col-sm-6">
					<div class="gallery__lft__inr p0 col-sm-4">
						<div class="gallery__lft__images">
							<div class="gallery__lft__img3">
								<img src="images/showsGallery1.jpg" alt="famous shows gallery">
							</div>
						</div>
					</div>
					<div class="gallery__lft__inr p0 col-sm-4">
						<div class="gallery__lft__images">
							<div class="gallery__inr__img3">
								<img src="images/showsGallery2.jpg" alt="famous shows gallery">
							</div>
						</div>
					</div>
					<div class="gallery__lft__inr p0 col-sm-4">
						<div class="gallery__lft__images">
							<div class="gallery__inr__img3">
								<img src="images/showsGallery3.jpg" alt="famous shows gallery">
							</div>
						</div>
					</div>
				</div>
				<div class="shows__gallery__rgt p0 col-sm-6">
					<div class="gallery__rgt__inr gallery__rgt__inr__topImg p0 col-sm-12">
						<div class="gallery__rgt__images">
							<div class="gallery__rgt__img1">
								<img src="images/showsGallery4.jpg" alt="famous shows gallery">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="shows__gallery__box__inr p0 col-sm-12">
				<div class="shows__gallery__lft p0 col-sm-6">
					<div class="gallery__lft__inr gallery__lft__inr__leftheight p0 col-sm-4">
						<div class="gallery__lft__images">
							<div class="gallery__lft__img3">
								<img src="images/showsGallery5.jpg" alt="famous shows gallery">
							</div>
						</div>
					</div>
					<div class="gallery__lft__inr gallery__lft__inr75r p0 col-sm-8">
						<div class="gallery__lft__images">
							<div class="gallery__inr__img3">
								<img src="images/showsGallery6.jpg" alt="famous shows gallery">
							</div>
						</div>
					</div>
					<div class="gallery__lft__inr p0 col-sm-4">
						<div class="gallery__lft__images">
							<div class="gallery__inr__img3">
								<img src="images/showsGallery7.jpg" alt="famous shows gallery">
							</div>
						</div>
					</div>
					<div class="gallery__lft__inr gallery__lft__inr75l p0 col-sm-8">
						<div class="gallery__lft__images ">
							<div class="gallery__inr__img3">
								<img src="images/showsGallery8.jpg" alt="famous shows gallery">
							</div>
						</div>
					</div>
					<div class="gallery__lft__inr gallery__lft__inr__rgtheight p0 col-sm-4">
						<div class="gallery__lft__images">
							<div class="gallery__inr__img3">
								<img src="images/showsGallery9.jpg" alt="famous shows gallery">
							</div>
						</div>
					</div>
					
					<div class="clearfix"></div>
				</div>
				
				<div class="shows__gallery__lft p0 col-sm-6">
					
				</div>
				<div class="shows__gallery__rgt p0 col-sm-6">
					<div class="gallery__rgt__inr p0 col-sm-12">
						<div class="gallery__rgt__cont">
							<div class="gallery__rgt__desc">
								<i class="fa fa-quote-left"></i>
								<h4>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's</h4>
							</div>
						</div>
						<div class="gallery__rgt__inr2 p0 col-sm-12">
						<div class="gallery__rgt__images">
							<div class="gallery__rgt__img1">
								<img src="images/showsGallery10.jpg" alt="famous shows gallery">
							</div>
						</div>
					</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</section>

<section class="route-area clrlist">
	<div class="container">
		<div class="hed">
			<h4>WE PROVIDE AWESOME DEALS</h4>
			<h2>Example <span>Route</span></h2>
			<p>Lorem Ipsum is simply dummy text and type setting</p>
		</div>
		<div class="clearfix"></div>
	</div>	
		<div class="route__box bg-cvr">
			<div class="route__inr">
				<div id="route-indicator" class="carousel slide slider--pauseplay" data-ride="carousel" data---interval="false">
				  <!-- Indicators -->
				  


				  <!-- Wrapper for slides -->
				  <div class="carousel-inner">


					<div class="item active">
					  <div class="route__cont">
						<div class="route__title">
							<h3>Bubrovinik</h3>
						</div>
						<div class="route__desc">
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the</p>
						</div>
						<div class="route__images">
							<div class="route__img__inr">
								<img src="images/routeInr1.jpg" alt="route">
							</div>
							<div class="route__img__inr">
								<img src="images/routeInr2.jpg" alt="route">
							</div>
						</div>
					  </div>
					  <div class="clearfix"></div>
					 
					</div>

					<div class="item">
					 <div class="route__cont">
						<div class="route__title">
							<h3>Bubrovinik</h3>
						</div>
						<div class="route__desc">
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the</p>
						</div>
						<div class="route__images">
							<div class="route__img__inr">
								<img src="images/routeInr1.jpg" alt="route">
							</div>
							<div class="route__img__inr">
								<img src="images/routeInr2.jpg" alt="route">
							</div>
						</div>
					  </div>
					</div>
					

					<div class="item">
					  <div class="route__cont">
						<div class="route__title">
							<h3>Bubrovinik</h3>
						</div>
						<div class="route__desc">
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the</p>
						</div>
						<div class="route__images">
							<div class="route__img__inr">
								<img src="images/routeInr1.jpg" alt="route">
							</div>
							<div class="route__img__inr">
								<img src="images/routeInr2.jpg" alt="route">
							</div>
						</div>
					  </div>
					</div>
					
					

					


				  </div>

				  <a class="left carousel-control" href="#route-indicator" data-slide="prev">
					<span class="fa fa-angle-left"></span>
				  </a>
				  <a class="right carousel-control" href="#route-indicator" data-slide="next">
					<span class="fa fa-angle-right"></span>
				  </a>
				  
				  
				</div>
			</div>
		</div>
	
</section>

<section class="yoga-area clrlist">
	
	<div class="container">
		<div class="hed">
			<h4>WE PROVIDE AWESOME DEALS</h4>
			<h2>Yoga <span>Retreat</span></h2>
			<p>Lorem Ipsum is simply dummy text and type setting</p>
		</div>
		<div class="clearfix"></div>
		
		<div class="yoga__divs">
			<div class="yoga__services col-sm-4">
				<div class="yoga__box">
					<div class="yoga__inr">
						<div class="yoga__img">
							<img src="images/yogaInr1.jpg" alt="yoga">
						</div>
						<div class="yoga__title">
							<h3>Eco Lodge</h3>
						</div>
						<div class="yoga__desc">
							<p>x7 nights at a luxury wellness eco retreat. Stay in a tented eco-chic lodge, or upgrade to a boutique suite in the main house</p>
						</div>
					</div>
				</div>
			</div>
			<div class="yoga__services col-sm-4">
				<div class="yoga__box">
					<div class="yoga__inr">
						<div class="yoga__img">
							<img src="images/yogaInr2.jpg" alt="yoga">
						</div>
						<div class="yoga__title">
							<h3>YOGA</h3>
						</div>
						<div class="yoga__desc">
							<p>Morning and evening yoga and meditation sessions each day. Mats, blocks and belts provided, but you may wish to bring your own mat.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="yoga__services col-sm-4">
				<div class="yoga__box">
					<div class="yoga__inr">
						<div class="yoga__img">
							<img src="images/yogaInr3.jpg" alt="yoga">
						</div>
						<div class="yoga__title">
							<h3>MEALS</h3>
						</div>
						<div class="yoga__desc">
							<p>7 breakfasts, 6 lunches and 3 freshly-prepared onboard dinners. The healthy Mediterranean cuisine is locally sourced and organic. All diets catered for.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="yoga__divs">
			<div class="yoga__services col-sm-4 col-sm-offset-2">
				<div class="yoga__box">
					<div class="yoga__inr">
						<div class="yoga__img">
							<img src="images/yogaInr4.jpg" alt="yoga">
						</div>
						<div class="yoga__title">
							<h3>Eco Lodge</h3>
						</div>
						<div class="yoga__desc">
							<p>Daily meditation sessions</p>
						</div>
					</div>
				</div>
			</div>
			<div class="yoga__services col-sm-4">
				<div class="yoga__box">
					<div class="yoga__inr">
						<div class="yoga__img">
							<img src="images/yogaInr5.jpg" alt="yoga">
						</div>
						<div class="yoga__title">
							<h3>YOGA</h3>
						</div>
						<div class="yoga__desc">
							<p>Private airport pick-up and drop-off. All in country transfers.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="yoga__details col-sm-12">
			<div class="yoga__paragraphs">
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
				<p>*accessible via direct flight from most East European airports.</p>
			</div>
			<div class="yoga__paragraphs yoga__paragraphs--two">
				<h4>Accomodation:</h4>
				<h5>Eco Lodges:</h5>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
				<h5>Suites:</h5>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
				<h5>Eco:</h5>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
			</div>
		</div>
		
	</div>	
	
	
</section>

<section class="schedule-area clrlist">
	<div class="container">
		<div class="hed">
			<h4>WE PROVIDE AWESOME DEALS</h4>
			<h2>FESTIVALS <span>SCHEDULES</span></h2>
			<p>Lorem Ipsum is simply dummy text and type setting</p>
		</div>
		<div class="clearfix"></div>
			<div class="schedule__box col-sm-12">
			<div class="sch__days col-sm-2">
				<ul class="nav nav-tabs ">
				  <li class="active">
					<a href="#day1" data-toggle="tab">
						<span class="sch__day__text">Day</span>
						<h2 class="sch__day__num">01</h2>
						<span class="sch__day__date">NOV 09, 16</span>
					</a>
				  </li>
				  <li>
					  <a href="#day2" data-toggle="tab">
						<span class="sch__day__text">Day</span>
						<h2 class="sch__day__num">02</h2>
						<span class="sch__day__date">NOV 09, 16</span>
					  </a>
				  </li>
				  <li>
					  <a href="#day3" data-toggle="tab">
						<span class="sch__day__text">Day</span>
						<h2 class="sch__day__num">03</h2>
						<span class="sch__day__date">NOV 09, 16</span>
					  </a>
				  </li>
				  <li>
					  <a href="#day4" data-toggle="tab">
						<span class="sch__day__text">Day</span>
						<h2 class="sch__day__num">04</h2>
						<span class="sch__day__date">NOV 09, 16</span>
					  </a>
				  </li>
				  <li>
					  <a href="#day5" data-toggle="tab">
						<span class="sch__day__text">Day</span>
						<h2 class="sch__day__num">05</h2>
						<span class="sch__day__date">NOV 09, 16</span>
					  </a>
				  </li>
				</ul>
			</div>

			<div class="sch__halls col-sm-10">
				<div class="tab-content">
				  <div class="tab-pane active" id="day1">
					
					  <!-- Nav tabs -->
					<ul class="nav nav-tabs">
					  <li class="active"><a href="#day1halltab1" data-toggle="tab">Hall A</a></li>
					  <li><a href="#day1halltab2" data-toggle="tab">Hall B</a></li>
					  <li><a href="#day1halltab3" data-toggle="tab">Hall C</a></li>
					  <li><a href="#day1halltab4" data-toggle="tab">Hall D</a></li>
					</ul>

					<!-- Tab panes -->
					<div class="tab-content">
					  <div class="tab-pane active" id="day1halltab1">
						<div class="sch__hall__days">
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr1.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												09:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr2.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												10:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr3.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
						</div>
					  </div>
					  <div class="tab-pane" id="day1halltab2">
						<div class="sch__hall__days">
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												11:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr1.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr2.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												09:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr3.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
						</div>
					  </div>
					  <div class="tab-pane" id="day1halltab3">
						<div class="sch__hall__days">
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												10:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr1.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr2.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr3.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
						</div>
					  </div>
					  <div class="tab-pane" id="day1halltab4">
						<div class="sch__hall__days">
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr1.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr2.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr3.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
						</div>
					  </div>
					</div>
				  
				  
				  
				  
				  </div>
				  <div class="tab-pane" id="day2">
					<ul class="nav nav-tabs">
					  <li class="active"><a href="#day2halltab1" data-toggle="tab">Hall A</a></li>
					  <li><a href="#day2halltab2" data-toggle="tab">Hall B</a></li>
					  <li><a href="#day2halltab3" data-toggle="tab">Hall C</a></li>
					  <li><a href="#day2halltab4" data-toggle="tab">Hall D</a></li>
					</ul>

					<!-- Tab panes -->
					<div class="tab-content">
					  <div class="tab-pane active" id="day2halltab1">
						<div class="sch__hall__days">
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr1.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr2.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr3.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
						</div>
					  </div>
					  <div class="tab-pane" id="day2halltab2">
						<div class="sch__hall__days">
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr1.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr2.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr3.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
						</div>
					  </div>
					  <div class="tab-pane" id="day2halltab3">
						<div class="sch__hall__days">
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr1.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr2.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr3.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
						</div>
					  </div>
					  <div class="tab-pane" id="day2halltab4">
						<div class="sch__hall__days">
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												09:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr1.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr2.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr3.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
						</div>
					  </div>
					</div>
				  </div>
				  
				   <div class="tab-pane" id="day3">
					<ul class="nav nav-tabs">
					  <li class="active"><a href="#day3halltab1" data-toggle="tab">Hall A</a></li>
					  <li><a href="#day3halltab2" data-toggle="tab">Hall B</a></li>
					  <li><a href="#day3halltab3" data-toggle="tab">Hall C</a></li>
					  <li><a href="#day3halltab4" data-toggle="tab">Hall D</a></li>
					</ul>

					<!-- Tab panes -->
					<div class="tab-content">
					  <div class="tab-pane active" id="day3halltab1">
						<div class="sch__hall__days">
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr1.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr2.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr3.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
						</div>
					  </div>
					  <div class="tab-pane" id="day3halltab2">
						<div class="sch__hall__days">
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr1.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr2.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												11:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr3.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
						</div>
					  </div>
					  <div class="tab-pane" id="day3halltab3">
						<div class="sch__hall__days">
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr1.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr2.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr3.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
						</div>
					  </div>
					  <div class="tab-pane" id="day3halltab4">
						<div class="sch__hall__days">
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr1.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr2.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr3.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
						</div>
					  </div>
					</div>
				  </div>
				  
				 <div class="tab-pane" id="day4">
					<ul class="nav nav-tabs">
					  <li class="active"><a href="#day4halltab1" data-toggle="tab">Hall A</a></li>
					  <li><a href="#day4halltab2" data-toggle="tab">Hall B</a></li>
					  <li><a href="#day4halltab3" data-toggle="tab">Hall C</a></li>
					  <li><a href="#day4halltab4" data-toggle="tab">Hall D</a></li>
					</ul>

					<!-- Tab panes -->
					<div class="tab-content">
					  <div class="tab-pane active" id="day4halltab1">
						<div class="sch__hall__days">
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr1.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr2.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr3.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
						</div>
					  </div>
					  <div class="tab-pane" id="day4halltab2">
						<div class="sch__hall__days">
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr1.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr2.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr3.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
						</div>
					  </div>
					  <div class="tab-pane" id="day4halltab3">
						<div class="sch__hall__days">
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr1.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr2.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr3.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
						</div>
					  </div>
					  <div class="tab-pane" id="day4halltab4">
						<div class="sch__hall__days">
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr1.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr2.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr3.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
						</div>
					  </div>
					</div>
				  </div>
				  
				 <div class="tab-pane" id="day5">
					<ul class="nav nav-tabs">
					  <li class="active"><a href="#day5halltab1" data-toggle="tab">Hall A</a></li>
					  <li><a href="#day5halltab2" data-toggle="tab">Hall B</a></li>
					  <li><a href="#day5halltab3" data-toggle="tab">Hall D</a></li>
					  <li><a href="#day5halltab4" data-toggle="tab">Hall D</a></li>
					</ul>

					<!-- Tab panes -->
					<div class="tab-content">
					  <div class="tab-pane active" id="day5halltab1">
						<div class="sch__hall__days">
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr1.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr2.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr3.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
						</div>
					  </div>
					  <div class="tab-pane" id="day5halltab2">
						<div class="sch__hall__days">
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr1.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr2.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr3.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
						</div>
					  </div>
					  <div class="tab-pane" id="day5halltab3">
						<div class="sch__hall__days">
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr1.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr2.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr3.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
						</div>
					  </div>
					  <div class="tab-pane" id="day5halltab4">
						<div class="sch__hall__days">
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr1.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr2.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
							<div class="sch__halls__event">
								<table>
									<tr>
										<td class="sch__halls__event__time">
											<div class="halls__event__time">
												08:30
											</div>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr class="sch__halls__desc__row">
										<td>
											<div class="halls__event__title">
												Lorem Ipsum is simply dummy text of the printing and industry.
											</div>
										</td>
										<td>
											<div class="hall__event__desc">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of  scrambled.
											</div>
										</td>
										<td>
											<div class="hall__event__img">
												<img src="images/hallInr3.jpg" alt="hall A">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="hall__event__share">
												<a href="#">
													<i class="fa fa-share-alt"></i>
												</a>
											</div>
										</td>
										<td>
											<div class="hall__event__presenter">
												Jgon Due / CEO at Croatia Mvement
											</div>
										</td>
										<td>
											
										</td>
									</tr>
									
								</table>
							</div>
						</div>
					  </div>
					</div>
				  </div>
				  
				</div>
			</div>
			<div class="schedule__download">
				<a href="#"><img src="images/scheduleDownload.jpg" alt="schedule download"></a>
			</div>
			<div class="clearfix"></div>
			</div>
	</div>
</section>

<section class="culture-trip-area culture-detail-trip-area">
		<div class="container">
			
				<div class="hed">
					<h4>WE PROVIDE AWESOME DEALS</h4>
					<h2>Customize Your <span>Trip</span></h2>
					<p>Top 10 Destination of Croatia</p>
				</div>
			
			<div class="clearfix"></div>
				
			<div class="sailbox cul__tripbox col-sm-4">
				<div class="sailbox__inr cul__tripbox__inr">
                    <div class="cul__trip__title">
                        <h3>Surf & Yoga Beach House</h3>
                    </div>
					<div class="sailbox__img cul__tripbox__img">
						<img src="images/sail1.jpg" alt="">
						<div class="sailbox__instant cul__tripbox__instant">INSTANT BOOK</div>
					</div>
					<div class="cul__tripbox__desc">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy</p>
					</div>
					<div class="sailbox__cont cul__tripbox__priceView">	
						<ul class="pul-lft">
							<li class="priceView__startFrom">Start From</li>
							<li class="priceView__price">$540.00</li>
						</ul>
						<ul class="pul-rgt priceView__viewBtn__list">
							<li class="priceView__viewBtn"><a href="#">View Details</a></li>
						</ul>
					</div>
				</div>
			</div>

			<div class="sailbox cul__tripbox col-sm-4">
				<div class="sailbox__inr cul__tripbox__inr">
                    <div class="cul__trip__title">
                        <h3>Croatian Ice Hotel</h3>
                    </div>
					<div class="sailbox__img cul__tripbox__img">
						<img src="images/sail2.jpg" alt="">
						<div class="sailbox__instant cul__tripbox__instant">INSTANT BOOK</div>
					</div>
					<div class="cul__tripbox__desc">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy</p>
					</div>
					<div class="sailbox__cont cul__tripbox__priceView">	
						<ul class="pul-lft">
							<li class="priceView__startFrom">Start From</li>
							<li class="priceView__price">$375.00</li>
						</ul>
						<ul class="pul-rgt priceView__viewBtn__list">
							<li class="priceView__viewBtn"><a href="#">View Details</a></li>
						</ul>
					</div>
				</div>
			</div>

			<div class="sailbox cul__tripbox col-sm-4">
				<div class="sailbox__inr cul__tripbox__inr">
                    <div class="cul__trip__title">
                        <h3>Body Balance Bali</h3>
                    </div>
					<div class="sailbox__img cul__tripbox__img">
						<img src="images/sail3.jpg" alt="">
						<div class="sailbox__instant cul__tripbox__instant">INSTANT BOOK</div>
					</div>
					<div class="cul__tripbox__desc">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy</p>
					</div>
					<div class="sailbox__cont cul__tripbox__priceView">	
						<ul class="pul-lft">
							<li class="priceView__startFrom">Start From</li>
							<li class="priceView__price">$555.00</li>
						</ul>
						<ul class="pul-rgt priceView__viewBtn__list">
							<li class="priceView__viewBtn"><a href="#">View Details</a></li>
						</ul>
					</div>
				</div>
			</div>

			<div class="sailbox cul__tripbox col-sm-4">
				<div class="sailbox__inr cul__tripbox__inr">
                    <div class="cul__trip__title">
                        <h3>Rock The Kabash</h3>
                    </div>
					<div class="sailbox__img cul__tripbox__img">
						<img src="images/sail4.jpg" alt="">
						<div class="sailbox__instant cul__tripbox__instant">INSTANT BOOK</div>
					</div>
					<div class="cul__tripbox__desc">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy</p>
					</div>
					<div class="sailbox__cont cul__tripbox__priceView">	
						<ul class="pul-lft">
							<li class="priceView__startFrom">Start From</li>
							<li class="priceView__price">$540.00</li>
						</ul>
						<ul class="pul-rgt priceView__viewBtn__list">
							<li class="priceView__viewBtn"><a href="#">View Details</a></li>
						</ul>
					</div>
				</div>
			</div>

			<div class="sailbox cul__tripbox col-sm-4">
				<div class="sailbox__inr cul__tripbox__inr">
                    <div class="cul__trip__title">
                        <h3>Yoga & Sailing</h3>
                    </div>
					<div class="sailbox__img cul__tripbox__img">
						<img src="images/sail5.jpg" alt="">
						<div class="sailbox__instant cul__tripbox__instant">INSTANT BOOK</div>
					</div>
					<div class="cul__tripbox__desc">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy</p>
					</div>
					<div class="sailbox__cont cul__tripbox__priceView">	
						<ul class="pul-lft">
							<li class="priceView__startFrom">Start From</li>
							<li class="priceView__price">$659.00</li>
						</ul>
						<ul class="pul-rgt priceView__viewBtn__list">
							<li class="priceView__viewBtn"><a href="#">View Details</a></li>
						</ul>
					</div>
				</div>
			</div>

			<div class="sailbox cul__tripbox col-sm-4">
				<div class="sailbox__inr cul__tripbox__inr">
                    <div class="cul__trip__title">
                        <h3>Tribe</h3>
                    </div>
					<div class="sailbox__img cul__tripbox__img">
						<img src="images/sail6.jpg" alt="">
						<div class="sailbox__instant cul__tripbox__instant">INSTANT BOOK</div>
					</div>
					<div class="cul__tripbox__desc">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy</p>
					</div>
					<div class="sailbox__cont cul__tripbox__priceView">	
						<ul class="pul-lft">
							<li class="priceView__startFrom">Start From</li>
							<li class="priceView__price">$255.00</li>
						</ul>
						<ul class="pul-rgt priceView__viewBtn__list">
							<li class="priceView__viewBtn"><a href="#">View Details</a></li>
						</ul>
					</div>
				</div>
			</div>
           
 
		</div>
	</section>
	
	<section class="trip-area trip-highlight-area imgzoom--hover">
		<div class="container">
			
				<div class="hed">
					<h4>WE PROVIDE AWESOME DEALS</h4>
					<h2>Trips <span>highlights</span></h2>
					<p>Lorem Ipsum is simply dummy text and typesetting</p>
				</div>

		
			<div class="clearfix"></div>


			<div class="tripbox tripbox__hightlights col-sm-3">
				<div class="tripbox__inr mb30">
					<div class="tripbox__img">
						<img src="images/tripHighlight1.jpg" alt="" />
					</div>
					<div class="tripbox__cont">
						<div class="cont hover valigner">
							<div class="valign"><h3>White Party</h3></div>
						</div>
					</div>
				</div>
			</div>
			


			<div class="tripbox tripbox__hightlights col-sm-3">
				<div class="tripbox__inr mb30">
					<div class="tripbox__img">
						<img src="images/tripHighlight2.jpg" alt="" />
					</div>
					<div class="tripbox__cont">
						<div class="cont hover valigner">
							<div class="valign"><h3>Seaside Yoga</h3></div>
						</div>
					</div>
				</div>
			</div>
			


			<div class="tripbox tripbox__hightlights col-sm-3">
				<div class="tripbox__inr mb30">
					<div class="tripbox__img">
						<img src="images/tripHighlight3.jpg" alt="" />
					</div>
					<div class="tripbox__cont">
						<div class="cont hover valigner">
							<div class="valign"><h3>Regatta Race</h3></div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="tripbox tripbox__hightlights col-sm-3">
				<div class="tripbox__inr mb30">
					<div class="tripbox__img">
						<img src="images/tripHighlight4.jpg" alt="" />
					</div>
					<div class="tripbox__cont">
						<div class="cont hover valigner">
							<div class="valign"><h3>Green Caves</h3></div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="tripbox tripbox__hightlights col-sm-3">
				<div class="tripbox__inr">
					<div class="tripbox__img">
						<img src="images/tripHighlight5.jpg" alt="" />
					</div>
					<div class="tripbox__cont">
						<div class="cont hover valigner">
							<div class="valign"><h3>Ancient Ruins</h3></div>
						</div>
					</div>
				</div>
			</div>
			


			<div class="tripbox tripbox__hightlights col-sm-3">
				<div class="tripbox__inr">
					<div class="tripbox__img">
						<img src="images/tripHighlight6.jpg" alt="" />
					</div>
					<div class="tripbox__cont">
						<div class="cont hover valigner">
							<div class="valign"><h3>Dinner at Oldest
Fort</h3></div>
						</div>
					</div>
				</div>
			</div>
			


			<div class="tripbox tripbox__hightlights col-sm-3">
				<div class="tripbox__inr">
					<div class="tripbox__img">
						<img src="images/tripHighlight7.jpg" alt="" />
					</div>
					<div class="tripbox__cont">
						<div class="cont hover valigner">
							<div class="valign"><h3>Beach Bar 
Parties</h3></div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="tripbox tripbox__hightlights col-sm-3">
				<div class="tripbox__inr">
					<div class="tripbox__img">
						<img src="images/tripHighlight8.jpg" alt="" />
					</div>
					<div class="tripbox__cont">
						<div class="cont hover valigner">
							<div class="valign"><h3>Floating Raft 
Parties</h3></div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</section>

	
<?php include("signup-area.php"); ?>
	
<script src="js/masonry.pkgd.min.js"></script>
	
<?php include("footer.php"); ?>