<title>Culture and Wellness</title>
<meta name="description" content="Culture and Wellness">

<?php include("header.php"); ?>

<section class="slider-area culture-wellness-area hover-ctrl no-ctrl" >
		<div id="carousel-example-generic" class="carousel slide slider--pauseplay" data-ride="carousel" data---interval="false">
		
		  <ol class="carousel-indicators thumbs">
			<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
			<li data-target="#carousel-example-generic" data-slide-to="1"></li>
			<li data-target="#carousel-example-generic" data-slide-to="2"></li>
		  </ol>

		  <!-- Wrapper for slides -->
		  <div class="carousel-inner">

				<div class="item active">
				
				  <img src="images/cultureBnr.jpg" alt="...">
				  <div class="container posrel">
					  <div class="caro-caps">
						<h1>CROATIAN CULTURE & WELLNESS</h1><br/>
						<p>Memories of The Cultural & Wellness Week</p>
						<div class="lnk-btn inline-block more-btn"><a href="#">LINK ROUTING</a></div>
					  </div>
				  </div>
				  
				  
				</div>

		  </div>

		  <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left"></span>
		  </a>
		  <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right"></span>
		  </a>
		  
		  
		</div>
		
		
</section>

<section class="famous-show festival-famous-show clrlist">
	<div class="container">
		<div class="hed">
			<h4>WE PROVIDE AWESOME DEALS</h4>
			<h2>Famous <span>shows</span></h2>
			<p>This Summer & Which One Will You Choose</p>
		</div>
		<div class="clearfix"></div>
		
		<div class="famshow__desc col-sm-12">
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktopLorem Ipsum.</p>
		</div>
		<div class="clearfix"></div>
		<div class="famshow__more">
			<a href="#">Explore</a>
		</div>
	</div>
</section>

<section class="festival-explore-area clrlist">
	<div class="container0">
		<div class="fest__exp__link p0 col-sm-6">
			<div class="fest__exp__box p0 col-sm-4">
				<div class="fest__exp__img">
					<img src="images/festExp1.jpg" alt="festival-post" />
					<div class="fest__exp__hover">
						<a href="#"><i class="fa fa-search"></i></a>
					</div>
				</div>
			</div>
			<div class="fest__exp__box p0 col-sm-4">
				<div class="fest__exp__img">
					<img src="images/festExp2.jpg" alt="festival-post" />
					<div class="fest__exp__hover">
						<a href="#"><i class="fa fa-search"></i></a>
					</div>
				</div>
			</div>
			<div class="fest__exp__box p0 col-sm-4">
				<div class="fest__exp__img">
					<img src="images/festExp3.jpg" alt="festival-post" />
					<div class="fest__exp__hover">
						<a href="#"><i class="fa fa-search"></i></a>
					</div>
				</div>
			</div>
			<div class="fest__exp__box p0 col-sm-4">
				<div class="fest__exp__img">
					<img src="images/festExp4.jpg" alt="festival-post" />
					<div class="fest__exp__hover">
						<a href="#"><i class="fa fa-search"></i></a>
					</div>
				</div>
			</div>
			<div class="fest__exp__box p0 col-sm-4">
				<div class="fest__exp__img">
					<img src="images/festExp5.jpg" alt="festival-post" />
					<div class="fest__exp__hover">
						<a href="#"><i class="fa fa-search"></i></a>
					</div>
				</div>
			</div>
			<div class="fest__exp__box p0 col-sm-4">
				<div class="fest__exp__img">
					<img src="images/festExp6.jpg" alt="festival-post" />
					<div class="fest__exp__hover">
						<a href="#"><i class="fa fa-search"></i></a>
					</div>
				</div>
			</div>
		</div>
		<div class="fest__exp__link p0 col-sm-6">
			<div class="fest__exp__box p0 col-sm-12">
				<div class="fest__exp__inr">
					<div class="fest__exp__video">
						<img src="images/festExpVideo.jpg" alt="festival-post" />
					</div>
					<div class="fest__video__cont">
						<div class="vidcont vidhover valigner">
							<div class="valign">
								<div class="fest__video__cont__inr">
									<div class="fest__video__icon">
										<a href="#" data-toggle="modal" data-target="#video2"><i class="fa fa-play"></i></a>
									</div>
									<div class="fest__video__title">
										<h2>STARTUP VIDEO</h2>
									</div>
									<div class="fest__video__desc">
										<h3>GREAT STARTUP OF BIGGEST 
FESTIVAL OF YEAR</h3>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Modal -->
<div id="video2" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
     
     
        <iframe src="https://player.vimeo.com/video/27909426" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

    
     
    </div>

  </div>
</div>

<section class="festival-upcoming-area clrlist">
	<div class="container">
		<div class="hed">
			<h4>WE PROVIDE AWESOME DEALS</h4>
			<h2>UPCOMING <span>Events</span></h2>
			<p>Lorem Ipsum is simply dummy text and typesetting</p>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="container0">
		
		<div class="fest-success p0 col-sm-12 bg-cvr valigner" style="background-image:url('images/festComing1.jpg');">
			
				
							<div class="fest-success__caps valign w100 white">
								
								<div class="fest-success__img">
									<img src="images/successLogo.png">
								</div>
								
								<h1>MOST SUCCESSFUL CULTURAL FESTIVAL</h1>
								
								<h6>Written on Friday, 04 Nov 2016 4:21</h6>
								
								<div class="fest__success__more">
									<a href="#">Explore</a>
								</div>
								
							</div>
						 
		</div>
		
		
		
		<div class="fest-success p0 col-sm-6 bg-cvr valigner" style="background-image:url('images/festComing2.jpg');">
			
				
							<div class="fest-success__caps fest__success--two valign w100 white">
								 
								<h1>MOST SUCCESSFUL CULTURAL FESTIVAL</h1>
								
								<h6>Written on Friday, 04 Nov 2016 4:21</h6>
								
								<div class="fest__success__more fest__success__more--two">
									<a href="#">Read More</a>
								</div>
								
							</div>
						 
		</div>
		
		
		
		
		<div class="fest-success p0 col-sm-6 bg-cvr valigner" style="background-image:url('images/festComing3.jpg');">
			
				
							<div class="fest-success__caps fest__success--two valign w100 white">
								 
								<h1>MOST SUCCESSFUL CULTURAL FESTIVAL</h1>
								
								<h6>Written on Friday, 04 Nov 2016 4:21</h6>
								
								<div class="fest__success__more fest__success__more--two">
									<a href="#">Read More</a>
								</div>
								
							</div>
						 
		</div>
		
		
		
		<div class="fest-success p0 col-sm-12 bg-cvr valigner" style="background-image:url('images/festComing4.jpg');">
			
				
							<div class="fest-success__caps valign w100 white">
								 
								<h1>MOST SUCCESSFUL CULTURAL FESTIVAL</h1>
								
								<h6>Written on Friday, 04 Nov 2016 4:21</h6>
								
								<div class="fest__success__more fest__success__more--two">
									<a href="#">Read More</a>
								</div>
								
							</div>
						 
		</div>
		
		
		
		

		
	</div>
	
</section>

<section class="culture-trip-area culture-detail-trip-area fest-trip-area">
		<div class="container">
			
				<div class="hed">
					<h4>WE PROVIDE AWESOME DEALS</h4>
					<h2>Customize Your <span>Trip</span></h2>
					<p>Top 10 Destination of Croatia</p>
				</div>
			
			<div class="clearfix"></div>
				
			<div class="sailbox cul__tripbox col-sm-4">
				<div class="sailbox__inr cul__tripbox__inr">
                    <div class="cul__trip__title">
                        <h3>Surf & Yoga Beach House</h3>
                    </div>
					<div class="sailbox__img cul__tripbox__img">
						<img src="images/sail1.jpg" alt="">
						<div class="sailbox__instant cul__tripbox__instant">INSTANT BOOK</div>
					</div>
					<div class="cul__tripbox__desc">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy</p>
					</div>
					<div class="sailbox__cont cul__tripbox__priceView">	
						<ul class="pul-lft">
							<li class="priceView__startFrom">Start From</li>
							<li class="priceView__price">$540.00</li>
						</ul>
						<ul class="pul-rgt priceView__viewBtn__list">
							<li class="priceView__viewBtn"><a href="#">View Details</a></li>
						</ul>
					</div>
				</div>
			</div>

			<div class="sailbox cul__tripbox col-sm-4">
				<div class="sailbox__inr cul__tripbox__inr">
                    <div class="cul__trip__title">
                        <h3>Croatian Ice Hotel</h3>
                    </div>
					<div class="sailbox__img cul__tripbox__img">
						<img src="images/sail2.jpg" alt="">
						<div class="sailbox__instant cul__tripbox__instant">INSTANT BOOK</div>
					</div>
					<div class="cul__tripbox__desc">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy</p>
					</div>
					<div class="sailbox__cont cul__tripbox__priceView">	
						<ul class="pul-lft">
							<li class="priceView__startFrom">Start From</li>
							<li class="priceView__price">$375.00</li>
						</ul>
						<ul class="pul-rgt priceView__viewBtn__list">
							<li class="priceView__viewBtn"><a href="#">View Details</a></li>
						</ul>
					</div>
				</div>
			</div>

			<div class="sailbox cul__tripbox col-sm-4">
				<div class="sailbox__inr cul__tripbox__inr">
                    <div class="cul__trip__title">
                        <h3>Body Balance Bali</h3>
                    </div>
					<div class="sailbox__img cul__tripbox__img">
						<img src="images/sail3.jpg" alt="">
						<div class="sailbox__instant cul__tripbox__instant">INSTANT BOOK</div>
					</div>
					<div class="cul__tripbox__desc">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy</p>
					</div>
					<div class="sailbox__cont cul__tripbox__priceView">	
						<ul class="pul-lft">
							<li class="priceView__startFrom">Start From</li>
							<li class="priceView__price">$555.00</li>
						</ul>
						<ul class="pul-rgt priceView__viewBtn__list">
							<li class="priceView__viewBtn"><a href="#">View Details</a></li>
						</ul>
					</div>
				</div>
			</div>

			<div class="sailbox cul__tripbox col-sm-4">
				<div class="sailbox__inr cul__tripbox__inr">
                    <div class="cul__trip__title">
                        <h3>Rock The Kabash</h3>
                    </div>
					<div class="sailbox__img cul__tripbox__img">
						<img src="images/sail4.jpg" alt="">
						<div class="sailbox__instant cul__tripbox__instant">INSTANT BOOK</div>
					</div>
					<div class="cul__tripbox__desc">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy</p>
					</div>
					<div class="sailbox__cont cul__tripbox__priceView">	
						<ul class="pul-lft">
							<li class="priceView__startFrom">Start From</li>
							<li class="priceView__price">$540.00</li>
						</ul>
						<ul class="pul-rgt priceView__viewBtn__list">
							<li class="priceView__viewBtn"><a href="#">View Details</a></li>
						</ul>
					</div>
				</div>
			</div>

			<div class="sailbox cul__tripbox col-sm-4">
				<div class="sailbox__inr cul__tripbox__inr">
                    <div class="cul__trip__title">
                        <h3>Yoga & Sailing</h3>
                    </div>
					<div class="sailbox__img cul__tripbox__img">
						<img src="images/sail5.jpg" alt="">
						<div class="sailbox__instant cul__tripbox__instant">INSTANT BOOK</div>
					</div>
					<div class="cul__tripbox__desc">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy</p>
					</div>
					<div class="sailbox__cont cul__tripbox__priceView">	
						<ul class="pul-lft">
							<li class="priceView__startFrom">Start From</li>
							<li class="priceView__price">$659.00</li>
						</ul>
						<ul class="pul-rgt priceView__viewBtn__list">
							<li class="priceView__viewBtn"><a href="#">View Details</a></li>
						</ul>
					</div>
				</div>
			</div>

			<div class="sailbox cul__tripbox col-sm-4">
				<div class="sailbox__inr cul__tripbox__inr">
                    <div class="cul__trip__title">
                        <h3>Tribe</h3>
                    </div>
					<div class="sailbox__img cul__tripbox__img">
						<img src="images/sail6.jpg" alt="">
						<div class="sailbox__instant cul__tripbox__instant">INSTANT BOOK</div>
					</div>
					<div class="cul__tripbox__desc">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy</p>
					</div>
					<div class="sailbox__cont cul__tripbox__priceView">	
						<ul class="pul-lft">
							<li class="priceView__startFrom">Start From</li>
							<li class="priceView__price">$255.00</li>
						</ul>
						<ul class="pul-rgt priceView__viewBtn__list">
							<li class="priceView__viewBtn"><a href="#">View Details</a></li>
						</ul>
					</div>
				</div>
			</div>
           
 
		</div>
	</section>





<section class="fest-gallery-area clrlist">
    <div class="container">
        <div class="hed">
            <h4>WE PROVIDE AWESOME DEALS</h4>
            <h2>Our <span>Galleries</span></h2>
            <p>Lorem Ipsum is simply dummy text and typesetting</p>
		</div>
		<div class="clearfix"></div>
    </div>   
        <div class="fest-gallery-images list-col-5">
            <ul>
                <li><img src="images/gallery1.jpg" alt=""></li>
                <li><img src="images/gallery2.jpg" alt=""></li>
                <li><img src="images/gallery3.jpg" alt=""></li>
                <li><img src="images/gallery4.jpg" alt=""></li>
                <li><img src="images/gallery5.jpg" alt=""></li>
                <li><img src="images/gallery6.jpg" alt=""></li>
                <li><img src="images/gallery7.jpg" alt=""></li>
                <li><img src="images/gallery8.jpg" alt=""></li>
                <li><img src="images/gallery9.jpg" alt=""></li>
                <li><img src="images/gallery10.jpg" alt=""></li>
            </ul>
		</div>
		<div class="clearfix"></div>
  
</section>

<?php include("signup-area.php"); ?>

<?php include("footer.php"); ?>