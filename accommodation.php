<title>Accommodation</title>
<meta name="description" content="Accommodation">

<?php include("header.php"); ?>

<section class="accom-bnr-area">
		<div class="accom-bg accom-bnr">
			<img src="images/accomBnr.jpg" alt=""/>
		</div>	
		<div class="container">
			 <div class="accom-bnr-title text-center">
				 <h2>DISCOVER BEST VACATION RENTALS</h2> 
			 </div>
			 <div class="accom__search col-sm-10 col-sm-offset-1">
				<div class="accom__search__box fnc-select">
					<form class="form-inline">
					  <div class="form-group">
						<div class="input-group col-sm-4">
						  <div class="input-group-addon">
							<i class="fa fa-map-marker"></i>
						  </div>
						  <input type="text" class="form-control" id="exampleInputAmount" placeholder="Where are you going?">
						</div>
						<div class="input-group col-sm-2">
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						    <input type="text" class="form-control" data-provide="datepicker" placeholder="Check In">
						</div>
						<div class="input-group col-sm-2">
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						    <input type="text" class="form-control" data-provide="datepicker" placeholder="Check Out">
						</div>
						<div class="input-group col-sm-2">
							<span class="input-group-addon">
								<i class="fa fa-male"></i>
							</span>
							<select type="text" class="form-control">
								<option class="option">Guests</option>
								<option class="option">1</option>
								<option class="option">2</option>
								<option class="option">3</option>
							</select>
						</div>
						<div class="input-group accom__search__btn col-sm-2">
							<button type="submit" class="btn btn-primary">Search</button>
						</div>
					  </div>
					</form>
				</div>
			 </div>
		</div>
</section>

<section class="vacation-colset-area clrlist">

	<div class="container">
		<div class="vacation__box col-sm-4">
			<div class="vacation__inr">
				<div class="vacation__img">
					<img src="images/accomVac1.png" alt="accomodation search">
				</div>
				<div class="vacation__title">
					<h3>Search & Explore</h3>
				</div>
				<div class="vacation__desc">
					<p>Find accommodation that suits your budget and 
style.</p>
				</div>
			</div>
		</div>
		<div class="vacation__box col-sm-4">
			<div class="vacation__inr">
				<div class="vacation__img">
					<img src="images/accomVac2.png" alt="accomodation search">
				</div>
				<div class="vacation__title">
					<h3>Contact & Book</h3>
				</div>
				<div class="vacation__desc">
					<p>Contact Potential host, confirm travel dates and
book securely </p>
				</div>
			</div>
		</div>
		<div class="vacation__box col-sm-4">
			<div class="vacation__inr">
				<div class="vacation__img">
					<img src="images/accomVac3.png" alt="accomodation search">
				</div>
				<div class="vacation__title">
					<h3>Search & Explore</h3>
				</div>
				<div class="vacation__desc">
					<p>Find accommodation that suits your budget and 
style.</p>
				</div>
			</div>
		</div>
	</div>

</section>

<section class="accom-dest-area clrlist">
	<div class="container">
		<div class="hed">
			<h4>WE PROVIDE AWESOME DEALS</h4>
			<h2>Top <span>destination</span></h2>
			<p>Top 10 Destination of Croatia</p>
		</div>
		<div class="clearfix"></div>
		<div class="accom__dest__box col-sm-4">
			<div class="accom__dest__inr mb30">
				<div class="tripbox__img accom__dest__img">
					<img src="images/accomDest1.jpg" alt="" />
				</div>
				<div class="tripbox__cont accom__dest__cont">
					<div class="cont hover valigner">
						<div class="valign">
							<h3>Dubrovnik</h3>
						</div>
					</div>
					<div class="cont--two hover--two valigner">
						<div class="valign">
							<div class="cont--two__content">
								<h4>Dubrovnik</h4>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard scrambled it to make book.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="accom__dest__box col-sm-4">
			<div class="accom__dest__inr mb30">
				<div class="tripbox__img accom__dest__img">
					<img src="images/accomDest2.jpg" alt="" />
				</div>
				<div class="tripbox__cont accom__dest__cont">
					<div class="cont hover valigner">
						<div class="valign">
							<h3>Zagreb</h3>
						</div>
					</div>
					<div class="cont--two hover--two valigner">
						<div class="valign">
							<div class="cont--two__content">
								<h4>Zagreb</h4>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard scrambled it to make book.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="accom__dest__box col-sm-4">
			<div class="accom__dest__inr mb30">
				<div class="tripbox__img accom__dest__img">
					<img src="images/accomDest3.jpg" alt="" />
				</div>
				<div class="tripbox__cont accom__dest__cont">
					<div class="cont hover valigner">
						<div class="valign">
							<h3>Split</h3>
						</div>
					</div>
					<div class="cont--two hover--two valigner">
						<div class="valign">
							<div class="cont--two__content">
								<h4>Split</h4>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard scrambled it to make book.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="accom__dest__box col-sm-6">
			<div class="accom__dest__inr mb30">
				<div class="tripbox__img accom__dest__img">
					<img src="images/accomDest4.jpg" alt="" />
				</div>
				<div class="tripbox__cont accom__dest__cont">
					<div class="cont hover valigner">
						<div class="valign">
							<h3>Zadar</h3>
						</div>
					</div>
					<div class="cont--two hover--two valigner">
						<div class="valign">
							<div class="cont--two__content">
								<h4>Zadar</h4>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard scrambled it to make book.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="accom__dest__box col-sm-6">
			<div class="accom__dest__inr mb30">
				<div class="tripbox__img accom__dest__img">
					<img src="images/accomDest5.jpg" alt="" />
				</div>
				<div class="tripbox__cont accom__dest__cont">
					<div class="cont hover valigner">
						<div class="valign">
							<h3>Rijeka</h3>
						</div>
					</div>
					<div class="cont--two hover--two valigner">
						<div class="valign">
							<div class="cont--two__content">
								<h4>Rijeka</h4>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard scrambled it to make book.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="accom__dest__box col-sm-4">
			<div class="accom__dest__inr">
				<div class="tripbox__img accom__dest__img">
					<img src="images/accomDest6.jpg" alt="" />
				</div>
				<div class="tripbox__cont accom__dest__cont">
					<div class="cont hover valigner">
						<div class="valign">
							<h3>Pula</h3>
						</div>
					</div>
					<div class="cont--two hover--two valigner">
						<div class="valign">
							<div class="cont--two__content">
								<h4>Pula</h4>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard scrambled it to make book.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="accom__dest__box col-sm-4">
			<div class="accom__dest__inr">
				<div class="tripbox__img accom__dest__img">
					<img src="images/accomDest7.jpg" alt="" />
				</div>
				<div class="tripbox__cont accom__dest__cont">
					<div class="cont hover valigner">
						<div class="valign">
							<h3>Osijek</h3>
						</div>
					</div>
					<div class="cont--two hover--two valigner">
						<div class="valign">
							<div class="cont--two__content">
								<h4>Osijek</h4>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard scrambled it to make book.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="accom__dest__box col-sm-4">
			<div class="accom__dest__inr">
				<div class="tripbox__img accom__dest__img">
					<img src="images/accomDest8.jpg" alt="" />
				</div>
				<div class="tripbox__cont accom__dest__cont">
					<div class="cont hover valigner">
						<div class="valign">
							<h3>Rovinj</h3>
						</div>
					</div>
					<div class="cont--two hover--two valigner">
						<div class="valign">
							<div class="cont--two__content">
								<h4>Rovinj</h4>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard scrambled it to make book.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="accom-map-area clrlist">
	<div class="container0">
		<div class="accom__map">
			<img src="images/accomMap.jpg" alt=" ">
		</div>
	</div>
</section>

<section class="vacation-colset-area vacation-services-area clrlist">

	<div class="container">
		<div class="vacation__box col-sm-4">
			<div class="vacation__inr">
				<div class="vacation__img">
					<img src="images/accomSrv1.png" alt="accomodation search">
				</div>
				<div class="vacation__title">
					<h3>Value for money</h3>
				</div>
				<div class="vacation__desc">
					<p>Stay in a local home instead of an expensive
 hotel. </p>
				</div>
			</div>
		</div>
		<div class="vacation__box col-sm-4">
			<div class="vacation__inr">
				<div class="vacation__img">
					<img src="images/accomSrv2.png" alt="accomodation search">
				</div>
				<div class="vacation__title">
					<h3>Enjoy more space </h3>
				</div>
				<div class="vacation__desc">
					<p>For the same cost as a hotel room, you can rent an
 entire home, which means you get access to 
more space.</p>
				</div>
			</div>
		</div>
		<div class="vacation__box col-sm-4">
			<div class="vacation__inr">
				<div class="vacation__img">
					<img src="images/accomSrv3.png" alt="accomodation search">
				</div>
				<div class="vacation__title">
					<h3>Live like a local</h3>
				</div>
				<div class="vacation__desc">
					<p>Experience local life for richer, more authentic
travels.</p>
				</div>
			</div>
		</div>
		<div class="vacation__box col-sm-4">
			<div class="vacation__inr">
				<div class="vacation__img">
					<img src="images/accomSrv4.png" alt="accomodation search">
				</div>
				<div class="vacation__title">
					<h3>We are secure</h3>
				</div>
				<div class="vacation__desc">
					<p>We handle all payments for you using a secure 
online payment system. You can pay via credit 
card or paypal.</p>
				</div>
			</div>
		</div>
		<div class="vacation__box col-sm-4">
			<div class="vacation__inr">
				<div class="vacation__img">
					<img src="images/accomSrv5.png" alt="accomodation search">
				</div>
				<div class="vacation__title">
					<h3>Feel at home</h3>
				</div>
				<div class="vacation__desc">
					<p>Enjoy the conveniences of home. Cook breakfast 
in your own kitchen, use the laundry facilities
 or Wi-Fi without extra cost. </p>
				</div>
			</div>
		</div>
		<div class="vacation__box col-sm-4">
			<div class="vacation__inr">
				<div class="vacation__img">
					<img src="images/accomSrv6.png" alt="accomodation search">
				</div>
				<div class="vacation__title">
					<h3>More Choices</h3>
				</div>
				<div class="vacation__desc">
					<p>From private rooms to apartments, exotic villas or 
traditional homes, our list of accommodation is 
ever growing. </p>
				</div>
			</div>
		</div>
	</div>

</section>

<?php include("signup-area.php"); ?>

<?php include("footer.php"); ?>